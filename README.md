# plasma-icons

Plasma icons created for Plasma 5. If needed the icon set will be updated to Plasma 6 conventions.

## Breeze And Folders Dark

This is the standard Breeze Dark icon set but with a custom set of folder icons and the icons for the system file manager. All credits go to the creators of the original theme.

Similar to the original set, the icons respect the accent color.

Apply the theme:
- unpack the tar-file.
- move the "breeze-and-folders-dark" directory to $HOME/.local/share/icons/
- choose the icon theme in the system settings

If you experience issues with the icons or any inconsistencies, please contact me.
